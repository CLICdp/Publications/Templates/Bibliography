# Bibliography

Add it as a `git submodule` to your own repository

```
cd yourNoteFolder
git submodule add https://gitlab.cern.ch/CLICdp/Publications/Templates/Bibliography.git
git commit -m"Add bibliography submodule"
```

Now you can use or edit the files in the Bibliography. 
The files are tracked in a separate git repository.
So add your fork as a remote to the Bibliography and push changes and make merge requests.

In the repository of the note use 
```
git submodule foreach git pull origin master
git add Bibliography
git commit -m"Update Bibliography"
git push 
# etc
```
to propagate submodule changes to other authors.

## Continuous Integration


In the `.gitlab.yml` add at the top

```
variables:
  GIT_SUBMODULE_STRATEGY: recursive

```